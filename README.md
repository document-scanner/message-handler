# Message Handler
A simple collection of interfaces and simple implementations of a framework which allows passing messages to the user without being forced to choose between dialogs, status bars, file loggers or console output immediately.
