/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.message.handler;

import org.slf4j.Logger;

/**
 *
 * @author richter
 */
public class LoggerBugHandler implements BugHandler {
    @SuppressWarnings("PMD.LoggerIsNotStaticFinal")
    private final Logger logger;

    public LoggerBugHandler(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void handleUnexpectedException(ExceptionMessage message) {
        if(message == null) {
            throw new IllegalArgumentException("message mustn't be null");
        }
        logger.error("handling bug", message.getThrowable());
    }

    @Override
    public void shutdown() {
        //do nothing
    }
}
