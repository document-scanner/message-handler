/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.message.handler;

import javafx.scene.control.Alert;

/**
 *
 * @author richter
 */
public class JavaFXDialogBugHandler implements BugHandler {

    @Override
    public void handleUnexpectedException(ExceptionMessage message) {
        if(message == null) {
            throw new IllegalArgumentException("message mustn't be null");
        }
        DisplayUtils.displayOnJavaFXThread(() -> {
            Alert.AlertType type = JavaFXDialogIssueHandler.translateType(message.getType());
            Alert alert = new Alert(type, message.getText());
            alert.setTitle(message.getSummary());
            alert.showAndWait();
            return null;
        });
    }

    @Override
    public void shutdown() {
        //do nothing
    }
}
