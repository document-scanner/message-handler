/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.message.handler;

import java.awt.Frame;

/**
 *
 * @author richter
 */
public class DialogConfirmMessageHandler implements ConfirmMessageHandler {
    private final DialogDisplayer dialogDisplayer;

    public DialogConfirmMessageHandler(Frame parent,
            int textWidth,
            String titlePrefix,
            String titleSuffix) {
        this.dialogDisplayer = new DefaultDialogDisplayer(parent,
                textWidth,
                titlePrefix,
                titleSuffix);
    }

    public DialogConfirmMessageHandler(Frame parent) {
        this(parent,
                DialogDisplayer.TEXT_WIDTH_DEFAULT,
                "",
                "");
    }

    public DialogConfirmMessageHandler(Frame parent,
                String titlePrefix,
                String titleSuffix) {
        this(parent,
                DialogDisplayer.TEXT_WIDTH_DEFAULT,
                titlePrefix,
                titleSuffix);
    }

    @Override
    public String confirm(Message message,
            String... options) {
        return this.dialogDisplayer.displayDialog(message, options);
    }

    @Override
    public <C extends ConfirmOption> C confirm(Message message,
            C... options) {
        return this.dialogDisplayer.displayDialog(message,
                options);
    }

    @Override
    public int confirm(Message message) {
        return this.dialogDisplayer.displayYesNoDialog(message);
    }
}
