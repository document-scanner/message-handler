/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.message.handler;

/**
 *
 * @author richter
 */
public class DefaultIssueHandler implements IssueHandler {
    private final MessageHandler messageHandler;
    private final BugHandler bugHandler;

    public DefaultIssueHandler(MessageHandler messageHandler,
            BugHandler bugHandler) {
        this.messageHandler = messageHandler;
        this.bugHandler = bugHandler;
    }

    @Override
    public void handle(Message message) {
        messageHandler.handle(message);
    }

    @Override
    public void handleUnexpectedException(ExceptionMessage message) {
        messageHandler.handle(message);
        bugHandler.handleUnexpectedException(message);
    }

    @Override
    public void shutdown() {
        //do nothing
    }
}
