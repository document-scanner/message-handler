/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.message.handler;

import java.awt.Window;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 *
 * @author richter
 */
public class DialogBugHandler implements BugHandler {
    private final String bugReportingURL;
    private final DialogDisplayer dialogDisplayer;

    public DialogBugHandler(Window parent,
            String bugReportingURL,
            int textWidth,
            String titlePrefix,
            String titleSuffix) {
        this.dialogDisplayer = new DefaultDialogDisplayer(parent,
                textWidth,
                titlePrefix,
                titleSuffix);
        this.bugReportingURL = bugReportingURL;
    }

    public DialogBugHandler(Window parent,
            String bugReportingURL,
            String titlePrefix,
            String titleSuffix) {
        this(parent,
                bugReportingURL,
                DialogDisplayer.TEXT_WIDTH_DEFAULT,
                titlePrefix,
                titleSuffix);
    }

    public DialogBugHandler(Window parent,
            String bugReportingURL) {
        this(parent,
                bugReportingURL,
                DialogDisplayer.TEXT_WIDTH_DEFAULT,
                "", //titlePrefix
                "" //titleSuffix
        );
    }

    public DialogBugHandler(Window parent,
            String bugReportingURL,
            int textWidth) {
        this(parent,
                bugReportingURL,
                textWidth,
                "", //titlePrefix
                "" //titleSuffix
        );
    }

    @Override
    public void handleUnexpectedException(ExceptionMessage message) {
        String message0 = String.format("The following exception occured:"
                + " %s.\nPlease consider filing a bug at "
                + "<a href=\"%s\">%s</a>.\nStacktrace: %s",
                message.getText(),
                bugReportingURL,
                bugReportingURL,
                ExceptionUtils.getStackTrace(message.getThrowable()));
        dialogDisplayer.displayDialog(new Message(message0,
                message.getType(),
                message.getSummary()));
    }

    @Override
    public void shutdown() {
        //do nothing
    }
}
