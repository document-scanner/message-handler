/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.message.handler;

import javafx.scene.control.Alert;
import javax.swing.JOptionPane;

/**
 *
 * @author richter
 */
public class JavaFXDialogIssueHandler implements IssueHandler {
    private final JavaFXDialogMessageHandler messageHandler;
    private final JavaFXDialogBugHandler bugHandler;

    protected static Alert.AlertType translateType(int messageType) {
        switch(messageType) {
            case JOptionPane.ERROR_MESSAGE:
                return Alert.AlertType.ERROR;
            case JOptionPane.WARNING_MESSAGE:
                return Alert.AlertType.WARNING;
            case JOptionPane.INFORMATION_MESSAGE:
                return Alert.AlertType.INFORMATION;
            case JOptionPane.PLAIN_MESSAGE:
                return Alert.AlertType.NONE;
            case JOptionPane.QUESTION_MESSAGE:
                return Alert.AlertType.CONFIRMATION;
            default:
                throw new IllegalArgumentException(String.format("messageType %d not supported", messageType));
        }
    }

    public JavaFXDialogIssueHandler(JavaFXDialogMessageHandler messageHandler, JavaFXDialogBugHandler bugHandler) {
        this.messageHandler = messageHandler;
        this.bugHandler = bugHandler;
    }

    public JavaFXDialogIssueHandler() {
        this.messageHandler = new JavaFXDialogMessageHandler();
        this.bugHandler = new JavaFXDialogBugHandler();
    }


    @Override
    public void handle(Message message) {
        this.messageHandler.handle(message);
    }

    @Override
    public void handleUnexpectedException(ExceptionMessage message) {
        this.bugHandler.handleUnexpectedException(message);
    }

    @Override
    public void shutdown() {
        this.bugHandler.shutdown();
    }
}
