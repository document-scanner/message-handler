/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.message.handler;

/**
 *
 * @author richter
 */
public interface DialogDisplayer {
    int TEXT_WIDTH_DEFAULT = 200;

    /**
     * Displays a dialog as defined by implementation.
     *
     * @param message the message to display in the dialog
     */
    void displayDialog(Message message);

    /**
     * Displays a dialog as defined by implementation offering a selection
     * control for each of {@code options} and a cancel button.
     *
     * @param message the message to display in the dialog
     * @param options the options to choose from
     * @return the selected option of {@code options} or {@code null} if the
     *     dialog has been canceled
     */
    /*
    internal implementation notes:
    - Since most likely a JOptionPane will be used in an implementation, provide
    a separate method for enforcing yes-no-answers because JOptionPane doesn't
    allow that if options are provided (always has a OK and cancel button below
    the option choice)
    */
    String displayDialog(Message message,
            String... options);

    <C extends ConfirmOption> C displayDialog(Message message,
            C... options);

    /**
     * Displays a message and forces the user to choose a yes- or a no-option.
     * @param message the message to confirm
     * @return {@link javax.swing.JOptionPane#YES_OPTION} if the yes-option has been chosen,
     *     {@link javax.swing.JOptionPane#NO_OPTION} otherwise
     */
    /*
    internal implementation notes:
    - see internal implementation notes of displayDialog(Message,String...) for
    an explanation why this method is necessary
    */
    int displayYesNoDialog(Message message);
}
