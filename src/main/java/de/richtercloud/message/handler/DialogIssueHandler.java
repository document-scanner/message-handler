/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.message.handler;

import java.awt.Frame;

/**
 *
 * @author richter
 */
public class DialogIssueHandler implements IssueHandler {
    private final DialogMessageHandler messageHandler;
    private final DialogBugHandler bugHandler;

    public DialogIssueHandler(DialogMessageHandler messageHandler,
            DialogBugHandler bugHandler) {
        this.messageHandler = messageHandler;
        this.bugHandler = bugHandler;
    }

    public DialogIssueHandler(Frame parent,
            String bugReportingURL) {
        this.messageHandler = new DialogMessageHandler(parent);
        this.bugHandler = new DialogBugHandler(parent,
                bugReportingURL);
    }

    public DialogIssueHandler(Frame parent,
            String bugReportingURL,
            int textWidth) {
        this.messageHandler = new DialogMessageHandler(parent,
                textWidth);
        this.bugHandler = new DialogBugHandler(parent,
                bugReportingURL,
                textWidth);
    }

    public DialogIssueHandler(Frame parent,
            String bugReportingURL,
            String titlePrefix,
            String titleSuffix) {
        this.messageHandler = new DialogMessageHandler(parent,
                titlePrefix,
                titleSuffix);
        this.bugHandler = new DialogBugHandler(parent,
                bugReportingURL,
                titlePrefix,
                titleSuffix);
    }

    public DialogIssueHandler(Frame parent,
            String bugReportingURL,
            int textWidth,
            String titlePrefix,
            String titleSuffix) {
        this.messageHandler = new DialogMessageHandler(parent,
                textWidth,
                titlePrefix,
                titleSuffix);
        this.bugHandler = new DialogBugHandler(parent,
                bugReportingURL,
                textWidth,
                titlePrefix,
                titleSuffix);
    }

    @Override
    public void handle(Message message) {
        this.messageHandler.handle(message);
    }

    @Override
    public void handleUnexpectedException(ExceptionMessage message) {
        this.bugHandler.handleUnexpectedException(message);
    }

    @Override
    public void shutdown() {
        this.bugHandler.shutdown();
    }
}
