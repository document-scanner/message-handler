/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.message.handler;

import java.awt.Window;
import javax.swing.JOptionPane;

/**
 *
 * @author richter
 */
public class DefaultDialogDisplayer implements DialogDisplayer {
    private final static String MESSAGE_NULL_MESSAGE = "message mustn't be null";
    private final Window parent;
    private final int textWidth;
    private final String titlePrefix;
    private final String titleSuffix;

    public DefaultDialogDisplayer(Window parent, int textWidth, String titlePrefix, String titleSuffix) {
        this.parent = parent;
        this.textWidth = textWidth;
        this.titlePrefix = titlePrefix;
        this.titleSuffix = titleSuffix;
    }

    @Override
    public void displayDialog(Message message) {
        if(message == null) {
            throw new IllegalArgumentException(MESSAGE_NULL_MESSAGE);
        }
        DisplayUtils.displayOnEDT(() -> {
            JOptionPane.showMessageDialog(parent,
                    formatMessage(message.getText(),
                            textWidth),
                    generateDialogTitle(message.getSummary()), //title
                    message.getType() //messageType
            );
            return null;
        });
    }

    /**
     * Displays a dialog using a {@link JOptionPane}.
     * @param message the message to confirm
     * @param options the options to choose from
     * @return the selection option of {@code options}
     */
    @Override
    public String displayDialog(Message message,
            String... options) {
        if(message == null) {
            throw new IllegalArgumentException(MESSAGE_NULL_MESSAGE);
        }
        if(options == null) {
            throw new IllegalArgumentException("optionLabelMap mustn'be null");
        }
        if(options.length == 0) {
            throw new IllegalArgumentException("optionLabelMap mustn't be empty");
        }
        return DisplayUtils.displayOnEDT(() -> {
            return (String) JOptionPane.showInputDialog(parent,
                    formatMessage(message.getText(),
                            textWidth), //message
                    generateDialogTitle(message.getSummary()), //title
                    message.getType(),
                    null, //icon
                    options,
                    options[0] //initialValueSelection
            );
        });
    }

    @Override
    public ConfirmOption displayDialog(Message message,
            ConfirmOption... options) {
        if(message == null) {
            throw new IllegalArgumentException(MESSAGE_NULL_MESSAGE);
        }
        if(options == null) {
            throw new IllegalArgumentException("optionLabelMap mustn'be null");
        }
        if(options.length == 0) {
            throw new IllegalArgumentException("optionLabelMap mustn't be empty");
        }
        return DisplayUtils.displayOnEDT(() -> {
            return (ConfirmOption) JOptionPane.showInputDialog(parent,
                    formatMessage(message.getText(),
                            textWidth), //message
                    generateDialogTitle(message.getSummary()), //title
                    message.getType(),
                    null, //icon
                    options,
                    options[0] //initialValueSelection
            );
        });
    }

    @Override
    public int displayYesNoDialog(Message message) {
        if(message == null) {
            throw new IllegalArgumentException(MESSAGE_NULL_MESSAGE);
        }
        return DisplayUtils.displayOnEDT(() -> {
            return JOptionPane.showConfirmDialog(parent,
                    formatMessage(message.getText(),
                            textWidth),
                    generateDialogTitle(message.getSummary()), //title
                    JOptionPane.YES_NO_OPTION, //optionType
                    message.getType() //messageType
            );
        });
    }

    private String formatMessage(String message,
            int textWidth) {
        return String.format("<html><body width='%d'>%s</body></html>",
                textWidth,
                message);
    }

    private String generateDialogTitle(String messageSummary,
            String titlePrefix,
            String titleSuffix) {
        return String.format("%s%s%s",
                titlePrefix,
                messageSummary,
                titleSuffix);
    }

    private String generateDialogTitle(String messageSummary) {
        return generateDialogTitle(messageSummary,
                titlePrefix,
                titleSuffix);
    }
}
