/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.message.handler;

/**
 *
 * @author richter
 */
public interface ConfirmMessageHandler {

    /**
     * Implementations need to request input from the user and return
     * {@link javax.swing.JOptionPane#YES_OPTION} or {@link javax.swing.JOptionPane#NO_OPTION} which can
     * be used by callers.
     *
     * The choice of {@link javax.swing.JOptionPane#YES_OPTION} and
     * {@link javax.swing.JOptionPane#NO_OPTION} is for code reusage only and doesn't mean
     * that a {@link javax.swing.JOptionPane} needs to be used in the implementation.
     *
     * @param message the message to confirm
     * @return {@link javax.swing.JOptionPane#YES_OPTION} or {@link javax.swing.JOptionPane#NO_OPTION}
     */
    int confirm(Message message);

    /**
     * Implementations need to request input from the user and return one of
     * {@code options} values.
     *
     * @param message the message to confirm
     * @param options options to choose from
     * @return one of the specified {@code options} or {@code null} if the
     *     dialog has been canceled
     * @throws IllegalArgumentException if {@code options} is empty
     */
    /*
    internal implementation notes:
    - It doesn't make sense to provide integers for values and strings for
    description/labels since the strings presented to the user have to be
    disjoint in order for the choice to make any sense
    */
    String confirm(Message message,
            String... options);

    /**
     * Works like
     * {@link #confirm(de.richtercloud.message.handler.Message, java.lang.String...) },
     * but allows to work with enums which have to implement
     * {@link ConfirmOption} instead of plain strings.
     *
     * @param <C> the type of confirm options
     * @param message the message to confirm
     * @param options the options to choose from
     * @return one of the specified {@code options} or {@code null} if the
     *     dialog has been canceled
     */
    /*
    internal implementation notes:
    - figure out "possible heap pollution" warning
    */
    <C extends ConfirmOption> C confirm(Message message,
            C... options);
}
