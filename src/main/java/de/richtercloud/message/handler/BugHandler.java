/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.message.handler;

/**
 *
 * @author richter
 */
public interface BugHandler {

    /**
     * Handles an unexpected exception message which needs to be defined by the
     * implementation. This is different from
     * {@link #handleUnexpectedException(de.richtercloud.message.handler.ExceptionMessage) } in the way that
     * it should encourage the user to file a bug or confirm that the bug is
     * automatically filed (again dependening on the implementation).
     *
     * @param message the message to handle
     */
    /*
    internal implementation notes:
    - This should be a method here instead of being handled by passing a subtype
    of Message to handle(Message) because it allows to call the method on the
    interface in frameworks which cannot know their MessageHandler
    implementation details (including the bug reporting URL).
    */
    void handleUnexpectedException(ExceptionMessage message);

    /**
     * Closes eventually opened resources (network connection for upload of
     * exception stacktraces, etc.).
     */
    void shutdown();
}
